import Kotlin.kapt
import com.android.build.gradle.AppExtension
import org.jetbrains.kotlin.config.KotlinCompilerVersion

plugins {
    id(Android.application)
    id(Kotlin.Serialization.id)
    kotlin(Kotlin.android)
    kotlin(Kotlin.androidExtensions)
    kotlin(Kotlin.kapt)
}

buildscript {
    dependencies {
        classpath(kotlin(Kotlin.gradlePlugin, version = "${Kotlin.version}"))
        classpath(DexCount.dependency)
    }
}

configure<AppExtension> {
    defaultConfig {
        applicationId = Android.applicationId
        versionCode = Android.versionCode
        versionName = Android.versionName
    }
    
    signingConfigs {
        create("release") {
            storeFile = rootProject.file("keystore.jks")
            storePassword = System.getenv("ANDROID_KEYSTORE_PASSWORD")
            keyAlias = System.getenv("ANDROID_KEYSTORE_ALIAS")
            keyPassword = System.getenv("ANDROID_KEYSTORE_PRIVATE_KEY_PASSWORD")
        }
    }

    buildTypes {
        getByName("release") {
            resValue("string", "app_name", Android.appName)
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = true
            isZipAlignEnabled = true
            isDebuggable = false
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }

        getByName("debug") {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
            resValue("string", "app_name", "${Android.appName} (Dev)")
            isMinifyEnabled = false
            isDebuggable = true
            isShrinkResources = false
//            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    lintOptions {
        isAbortOnError = true
        isWarningsAsErrors = true
    }

    packagingOptions {
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/NOTICE.txt")
    }
}
/*
kapt {
    arguments {
        arg("moxyReflectorPackage", "com.softexlab.probation")
    }
}
*/

dependencies {
    implementation(project(Modules.data.name))
    implementation(project(Modules.domain.name))
    implementation(project(Modules.remote.name))
    implementation(project(Modules.cache.name))
    implementation(kotlin(Kotlin.stdLib, KotlinCompilerVersion.VERSION))
    implementation(Kotlin.Serialization.runtime)

    /* AndroidX */
    implementation(AndroidX.appcompatDependency)
    implementation(MaterialDesign.dependency)
    
    /* MVP MoxyX */
    kapt(MoxyX.compilerDependency)
    implementation(MoxyX.dependency)
    implementation(MoxyX.androidxDependency)
    /* DI Koin */
    implementation(Koin.androidDependency)
    /* Rx */
    implementation(Rx.rxKotlin)
    implementation(Rx.rxAndroid)
    /* RxBinding */
    implementation(Rx.RxBinding.appcompat)
    /* Glide */
    implementation(Glide.dependency)
    /* Okio */
    implementation(OkHttp.Okio.okio)
    
    /* Memory Tool LeakCanary */
    implementation(LeakCanary.leaksentry)
    debugImplementation(LeakCanary.dependency)

    /* Testing */
    testImplementation(JUnit.dependency)
}

apply {
    plugin(DexCount.plugin)
}
