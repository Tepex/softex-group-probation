# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Timber
-keepnames class timber.log.** { *; }

# AndroidX
-keepnames class androidx.appcompat.app.** { *; }
-keepnames class androidx.arch.core.executor.** { *; }
-keepnames class androidx.collection.** { *; }
-keepnames class androidx.constraintlayout.solver.widgets.** { *; }
-keepnames class androidx.core.** { *; }
-keepnames class androidx.customview.view.** { *; }
-keepnames class androidx.loader.app.** { *; }

# Material Design
-keepnames class com.google.android.material.** { *; }

# Koin
-keepnames class org.koin.** { *; }

# LeakCanary
-keepnames class leakcanary.RefWatcher

# Reflect
-keepnames class kotlin.reflect.jvm.** { *; }
-dontnote kotlinx.serialization.SerializationKt
-keep,includedescriptorclasses class com.softexlab.probation.**$$serializer { *; }
-keepclassmembers class com.softexlab.probation.** {
    *** Companion;
}
-keepclasseswithmembers class com.softexlab.probation.** {
    kotlinx.serialization.KSerializer serializer(...);
}

-keepnames class com.softexlab.probation.** { *; }
-keepnames class kotlinx.serialization.** { *; }
-keepnames class io.reactivex.rxkotlin.** { *; }
-keepnames class io.reactivex.android.schedulers.** { *; }
-keepnames class com.bumptech.glide.** { *; }

# Keep SourceFile names & Line Numbers for stack traces. (Note: If we are really security concious,
# we should remove this line.
-keepattributes SourceFile,LineNumberTable
-keepattributes *Annotation*, InnerClasses, Signature, Exception, EnclosingMethod

# Retrofit does reflection on method and parameter annotations.
-keepattributes RuntimeVisibleAnnotations, RuntimeVisibleParameterAnnotations

# Retain service method parameters when optimizing.
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}

# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# Ignore JSR 305 annotations for embedding nullability information.
-dontwarn javax.annotation.**

# Guarded by a NoClassDefFoundError try/catch and only used when on the classpath.
-dontwarn kotlin.Unit

# Top-level functions that can only be used by Kotlin.
-dontwarn retrofit2.KotlinExtensions

# With R8 full mode, it sees no subtypes of Retrofit interfaces since they are created with a Proxy
# and replaces all potential values with null. Explicitly keeping the interfaces prevents this.
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface <1>

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

-keepnames class okhttp3.** { *; }
-keepnames class okio.** { *; }
-keepnames class com.google.gson.** { *; }
-keepnames class retrofit2.** { *; }

#-keep class com.arellomobile.mvp.** { *; }
#-keep class org.conscrypt.** { *; }
