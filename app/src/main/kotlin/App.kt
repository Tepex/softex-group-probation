package com.softexlab.probation

import android.app.Application

import androidx.annotation.CallSuper

import com.softexlab.probation.cache.cacheModule
import com.softexlab.probation.remote.remoteModule

import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger

import org.koin.core.context.startKoin
import org.koin.core.logger.Level

import timber.log.Timber

class App : Application() {
    @CallSuper
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            if (BuildConfig.DEBUG) {
                androidLogger(Level.DEBUG)
            }

            androidContext(this@App)
            modules(
                mainModule,
                domainModule,
                dataModule,
                remoteModule,
                cacheModule
            )
        }
    }
}
