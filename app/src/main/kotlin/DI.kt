package com.softexlab.probation

import android.content.Context

import android.graphics.drawable.ColorDrawable

import androidx.core.content.ContextCompat

import com.softexlab.probation.data.Country
import com.softexlab.probation.data.CountryRepositoryImpl

import com.softexlab.probation.domain.CountriesInteractor
import com.softexlab.probation.domain.CountriesUseCase
import com.softexlab.probation.domain.CountryRepository

import com.softexlab.probation.remote.CountryRemoteImpl

import com.softexlab.probation.ui.SwipeCallback

import kotlinx.serialization.json.Json
import kotlinx.serialization.list

import org.koin.dsl.module

import timber.log.Timber

val mainModule = module {
    single { ColorDrawable(ContextCompat.getColor(get(), R.color.accent)) }
    single { SwipeCallback(get()) }
}

val domainModule = module {
    single<List<Country>> {

        val countries: List<Country>
        get<Context>().assets.open("countries.json").apply {
            countries = Json.parse(Country.serializer().list, readBytes().toString(Charsets.UTF_8))
        }.close()
        countries
    }

    single<CountriesUseCase> { CountriesInteractor(get()) }
}

val dataModule = module {
    single<CountryRepository> { CountryRepositoryImpl(get(), get()) }
}
