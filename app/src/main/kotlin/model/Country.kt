package com.softexlab.probation.model

data class Country(
    val id: String,
    var name: String,
    val imageUrl: String? = null
)
