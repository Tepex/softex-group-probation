package com.softexlab.probation.model

import com.softexlab.probation.domain.Country as CountryDomain

object CountryMapper {
    fun map(model: CountryDomain) =
        Country(
            id = model.id,
            name = model.name,
            imageUrl = model.imageUrl
        )
}
