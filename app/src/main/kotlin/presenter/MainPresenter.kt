package com.softexlab.probation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

import com.softexlab.probation.domain.CountriesUseCase
import com.softexlab.probation.domain.Country as CountryDomain
import com.softexlab.probation.model.CountryMapper
import com.softexlab.probation.view.MainView

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

import org.koin.core.KoinComponent
import org.koin.core.inject

import timber.log.Timber

@InjectViewState
class MainPresenter : MvpPresenter<MainView>(), KoinComponent {

    private val useCase: CountriesUseCase by inject()
    private val disposables = CompositeDisposable()
    private var scrollPosition: Int = 0

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadCountries(useCase.get())
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    fun refresh() {
        loadCountries(useCase.get(true))
    }

    fun remove(id: String, scrollPosition: Int) {
        this.scrollPosition = scrollPosition
        loadCountries(useCase.remove(id), false)
    }

    private fun loadCountries(single: Single<List<CountryDomain>>, useLoadingIndicator: Boolean = true) {
        if (useLoadingIndicator) viewState.toggleLoading(true)
        disposables.add(single
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { countries ->
                    Timber.d("presenter's got data")
                    viewState.setCountries(countries.map { CountryMapper.map(it) })
                    viewState.setScrollPosition(scrollPosition)
                    viewState.toggleLoading(false)
                },
                onError = { err ->
                    Timber.e(err)
                    viewState.toggleLoading(false)
                }
            )
        )
    }
}
