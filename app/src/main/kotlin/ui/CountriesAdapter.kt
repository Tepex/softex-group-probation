package com.softexlab.probation.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide

import com.softexlab.probation.R
import com.softexlab.probation.model.Country

import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.country_item.view.*

class CountriesAdapter(
    private val countries: List<Country>
) : RecyclerView.Adapter<CountriesAdapter.CountriesHolder>() {

    init {
        setHasStableIds(true)
    }

    fun getCountryId(position: Int) = countries[position].id

    override fun getItemCount() = countries.size

    override fun getItemId(position: Int) = getCountryId(position).hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CountriesHolder(LayoutInflater.from(parent.context).inflate(R.layout.country_item, parent, false))

    override fun onBindViewHolder(holder: CountriesHolder, pos: Int) = holder.bind(countries[pos])

    class CountriesHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(item: Country): Unit = with(containerView) {
            tvName.text = item.name
            if (item.imageUrl == null) {
                iv.isGone = true
            } else {
                iv.isVisible = true
                Glide.with(this).load(item.imageUrl).into(iv)
            }
        }
    }
}
