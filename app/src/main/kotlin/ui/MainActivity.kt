package com.softexlab.probation.ui

import android.os.Bundle

import android.view.Menu
import android.view.MenuItem

import androidx.annotation.CallSuper

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter

import com.jakewharton.rxbinding3.appcompat.itemClicks

import com.softexlab.probation.R
import com.softexlab.probation.model.Country
import com.softexlab.probation.presenter.MainPresenter
import com.softexlab.probation.view.MainView

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

import kotlinx.android.synthetic.main.activity_main.*

import org.koin.core.KoinComponent
import org.koin.core.inject

import timber.log.Timber

class MainActivity : MvpAppCompatActivity(), MainView, KoinComponent {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private val swipeCallback: SwipeCallback by inject()
    private var itemTouchHelper = ItemTouchHelper(swipeCallback)
    private val disposables = CompositeDisposable()
    private var refreshMenuItem: MenuItem? = null
    private var linearLayoutManager = LinearLayoutManager(this)

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setToolbar()
        setRecyclerView()
    }

    @CallSuper
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        refreshMenuItem = menu.findItem(R.id.action_refresh)
        return super.onCreateOptionsMenu(menu)
    }

    @CallSuper
    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    override fun toggleLoading(isLoading: Boolean) {
        Timber.d("loading: $isLoading")
        progressBar.isVisible = isLoading
        refreshMenuItem?.isEnabled = !isLoading
        rvCountries.isEnabled = !isLoading

        itemTouchHelper.attachToRecyclerView(if (!isLoading) rvCountries else null)
    }

    override fun setCountries(countries: List<Country>) {
        rvCountries.adapter = CountriesAdapter(countries)
    }

    override fun setScrollPosition(position: Int) {
        Timber.d("scroll position: $position")
        if (position > 0 && position < rvCountries.adapter?.itemCount ?: RecyclerView.NO_POSITION) {
            linearLayoutManager.scrollToPosition(position)
        }
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.setText(R.string.app_name)

        disposables.add(toolbar.itemClicks().subscribeBy { menuItem ->
            when (menuItem.itemId) {
                R.id.action_refresh -> presenter.refresh()
            }
        })
    }

    private fun setRecyclerView() {
        rvCountries.layoutManager = linearLayoutManager
        itemTouchHelper.attachToRecyclerView(rvCountries)

        disposables.add(swipeCallback.swipes().subscribeBy { position ->
            val adapter = rvCountries.adapter
            if (adapter is CountriesAdapter) {
                presenter.remove(
                    adapter.getCountryId(position),
                    linearLayoutManager.findFirstVisibleItemPosition()
                )
            }
        })
    }
}
