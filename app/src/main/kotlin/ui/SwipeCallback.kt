package com.softexlab.probation.ui

import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable

import androidx.annotation.CallSuper
import androidx.annotation.CheckResult

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

import org.koin.core.KoinComponent

class SwipeCallback(
    private val background: ColorDrawable
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT), KoinComponent {

    private val swipeSubject = PublishSubject.create<Int>()

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        swipeSubject.onNext(viewHolder.adapterPosition)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ) = false

    @CallSuper
    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        if (isCurrentlyActive) {
            val view = viewHolder.itemView
            background.setBounds(view.left, view.top, view.right, view.bottom)
        } else background.setBounds(0, 0, 0, 0)
        background.draw(c)
    }

    @CheckResult
    fun swipes(): Observable<Int> = swipeSubject
}
