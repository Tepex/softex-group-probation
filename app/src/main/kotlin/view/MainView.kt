package com.softexlab.probation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

import com.softexlab.probation.model.Country

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView {
    fun toggleLoading(isLoading: Boolean)
    fun setCountries(countries: List<Country>)
    fun setScrollPosition(position: Int)
}
