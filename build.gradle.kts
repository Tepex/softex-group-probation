import com.android.build.gradle.BaseExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath(BuildPlugins.dependency)
        classpath(kotlin(Kotlin.gradlePlugin, version = "${Kotlin.version}"))
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven(url = "https://jitpack.io")
    }
    
    
}

plugins {
    kotlin("jvm") version Kotlin.version
    id(Detekt.dependency) version Detekt.version
    id(BenManes.dependency) version BenManes.version
}

subprojects {
    afterEvaluate {
        val isAndroidApp = plugins.findPlugin(Kotlin.android) != null
        val isAndroidAppLibrary = plugins.findPlugin("android-library") != null
        
        if (isAndroidApp || isAndroidAppLibrary) {
            configure<BaseExtension> {
                compileSdkVersion(Android.compileSdk)
//                buildToolsVersion(Android.buildToolsVersion)
                defaultConfig {
                    minSdkVersion(Android.minSdk)
                    targetSdkVersion(Android.targetSdk)
                    testInstrumentationRunner = AndroidX.Test.instrumentationRunner
                }

                sourceSets {
                    getByName("main").java.srcDirs("src/main/kotlin")
                    getByName("test").java.srcDirs("src/test/kotlin")
                }

                compileOptions {
                    setSourceCompatibility(JavaVersion.VERSION_1_8)
                    setTargetCompatibility(JavaVersion.VERSION_1_8)
                }
            }

            /* Common dependencies for all Android modules */
            dependencies {
                // Logger Timber
                implementation(Timber.dependency)
            }
        }
    }
}

dependencies {
    detektPlugins(Detekt.formatting)
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        allWarningsAsErrors = true
    }
}

detekt {
    toolVersion = "${Detekt.version}"
    failFast = false
    filters = ".*build.*,.*/resources/.*,.*/tmp/.*"
    input = files(listOf(
            Modules.app.srcPath,
            Modules.data.srcPath,
            Modules.domain.srcPath,
            Modules.remote.srcPath,
            Modules.cache.srcPath
        )
    )
    config = files("detekt-config.yml")
}
