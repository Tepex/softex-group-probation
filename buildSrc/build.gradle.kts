import org.gradle.kotlin.dsl.`kotlin-dsl`
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}

sourceSets {
    named("main") {
        withConvention(KotlinSourceSet::class) {
            java.srcDirs("src/main/kotlin")
        }
    }
}

/*
Не выводить в консоль сообщение:
«The `kotlin-dsl` plugin applied to project ':buildSrc' enables experimental Kotlin compiler features…»
*/
kotlinDslPluginOptions {
    experimentalWarning.set(false)
}
