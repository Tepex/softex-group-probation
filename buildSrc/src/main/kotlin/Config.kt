class Module(val name: String) {
    companion object {
        const val src = "src/main/kotlin"
    }
    
    val path by lazy { name.replace(':', '/').substring(1) }
    val srcPath by lazy { "$path/$src" }
    
    override fun toString() = name
}

object Modules {
    val app = Module(":app")
    val domain = Module(":domain")
    val data = Module(":data")
    val remote = Module(":remote")
    val cache = Module(":cache")
}

object Kotlin {
    const val version           = "1.3.31"
    const val plugin            = "kotlin"
    const val android           = "android"
    const val androidExtensions = "android.extensions"
    const val kapt              = "kapt"
    
    const val stdLib               = "stdlib-jdk8"
    const val gradlePlugin         = "gradle-plugin"
    const val reflect              = "reflect"

    object Serialization {
        const val id      = "kotlinx-serialization"
//        const val version = "0.11.0"
        const val version = "0.11.0-1.3.30-eap-125"
        const val runtime = "org.jetbrains.kotlinx:kotlinx-serialization-runtime:$version"
    }
}

object BuildPlugins {
//    const val androidToolsVersion = "3.5.0-beta01"
    const val androidToolsVersion = "3.4.0"
    const val androidTools        = "com.android.tools.build:gradle"
    const val dependency          = "$androidTools:$androidToolsVersion"
}

object Android {
    const val application       = "com.android.application"
    const val library           = "com.android.library"
    const val applicationId     = "com.softexlab.probation"
    const val appName           = "Countries"
    const val buildToolsVersion = "28.0.3"
    const val compileSdk        = 28
    const val targetSdk         = 28
    const val minSdk            = 19
    const val versionCode       = 1
    const val versionName       = "1.0"
}

object AndroidX {
    const val appcompatVersion        = "1.1.0-alpha05"
    const val constraintLayoutVersion = "2.0.0-beta1"

    const val appcompatDependency        = "androidx.appcompat:appcompat:$appcompatVersion"
    const val constraintLayoutDependency = "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"

    object Test {
        const val runnerVersion         = "1.2.0-alpha04"
        const val espressoCoreVersion   = "3.2.0-alpha04"

        const val runner                = "androidx.test:runner:$runnerVersion"
        const val instrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        const val espressoCore          = "androidx.test.espresso:espresso-core:$espressoCoreVersion"
    }
}

object MaterialDesign {
    const val version    = "1.1.0-alpha06"
    const val dependency = "com.google.android.material:material:$version"
}

object DexCount {
    const val version    = "0.8.6"
    const val plugin     = "com.getkeepsafe.dexcount"
    const val dependency = "$plugin:dexcount-gradle-plugin:$version"
}

object BenManes {
    const val version    = "0.21.0"
    const val dependency = "com.github.ben-manes.versions"
}

object JUnit {
    const val version    = "4.13-beta-3"
    const val dependency = "junit:junit:$version"
}

object Timber {
    const val version    = "4.7.1"
    const val dependency = "com.jakewharton.timber:timber:$version"
}

object Koin {
    const val version           = "2.0.0-rc-3"
    const val coreDependency    = "org.koin:koin-core:$version"
    const val androidDependency = "org.koin:koin-android:$version"
}

object MoxyX {
    const val version            = "1.7.0"
    const val dependency         = "tech.schoolhelper:moxy-x:$version"
    const val androidxDependency = "tech.schoolhelper:moxy-x-androidx:$version"
    const val compilerDependency = "tech.schoolhelper:moxy-x-compiler:$version"
}

object Detekt {
    const val version    = "1.0.0-RC14"
    const val dependency = "io.gitlab.arturbosch.detekt"
    const val formatting = "$dependency:detekt-formatting:$version"
}

object LeakCanary {
    const val version    = "2.0-alpha-1"
    const val dependency = "com.squareup.leakcanary:leakcanary-android:$version"
    const val leaksentry = "com.squareup.leakcanary:leaksentry:$version"
}

object ThreeTen {
    const val version    = "1.2.0"
    const val dependency = "com.jakewharton.threetenabp:threetenabp:$version"
}

object Rx {
    const val rxJavaVersion    = "2.2.8"
    const val rxAndroidVersion = "2.1.1"
    const val rxKotlinVersion  = "2.3.0"
    const val rxJava           = "io.reactivex.rxjava2:rxjava:$rxJavaVersion"
    const val rxAndroid        = "io.reactivex.rxjava2:rxandroid:$rxAndroidVersion"
    const val rxKotlin         = "com.github.ReactiveX:RxKotlin:$rxKotlinVersion"
    
    object RxBinding {
        const val version      = "3.0.0-alpha2"
        const val group        = "com.jakewharton.rxbinding3"
        const val core         = "$group:rxbinding-core:$version"
        const val appcompat    = "$group:rxbinding-appcompat:$version"
        const val recyclerview = "$group:rxbinding-recyclerview:$version"
    }
}

object Glide {
    const val version    = "4.9.0"
    const val dependency = "com.github.bumptech.glide:glide:$version"
}

object Retrofit {
    const val version   = "2.5.0"
    const val core      = "com.squareup.retrofit2:retrofit:$version"
    const val converter = "com.squareup.retrofit2:converter-gson:$version"
    const val adapter   = "com.squareup.retrofit2:adapter-rxjava2:$version"
}

object Gson {
    const val version    = "2.8.5"
    const val dependency = "com.google.code.gson:gson:$version"
}

object OkHttp {
    const val version = "4.0.0-alpha01"
    const val okhttp  = "com.squareup.okhttp3:okhttp:$version"
    const val logging = "com.squareup.okhttp3:logging-interceptor:$version"
    
    object Okio {
        const val version = "2.2.2"
        const val okio    = "com.squareup.okio:okio:$version"
    }
}

object Room {
    const val version = "2.1.0-beta01"
    const val runtime = "androidx.room:room-runtime:$version"
    const val compile = "androidx.room:room-compiler:$version"
    const val rxjava2 = "androidx.room:room-rxjava2:$version"
    const val testing = "androidx.room:room-testing:$version"
}
