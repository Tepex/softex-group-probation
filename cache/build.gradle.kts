import org.jetbrains.kotlin.config.KotlinCompilerVersion

plugins {
    id(Android.library)
    kotlin(Kotlin.android)
    kotlin(Kotlin.kapt)
}

dependencies {
    implementation(kotlin(Kotlin.stdLib, KotlinCompilerVersion.VERSION))
    /* DI Koin */
    implementation(Koin.coreDependency)
    /* Room */
    implementation(Room.runtime)
    implementation(Room.rxjava2)
    kapt(Room.compile)
    /* Testing */
    testImplementation(JUnit.dependency)
    
    implementation(project(Modules.data.name))
}
