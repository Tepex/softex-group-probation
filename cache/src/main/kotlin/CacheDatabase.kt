package com.softexlab.probation.cache

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Country::class), version = 1, exportSchema = false)
abstract class CacheDatabase: RoomDatabase() {
    abstract fun countryCachedDao(): CountryDao
}
