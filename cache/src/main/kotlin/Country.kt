package com.softexlab.probation.cache

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ColumnInfo

import com.softexlab.probation.data.Country as CountryData

@Entity(tableName = CountryData.ENTITY_NAME)
data class Country(
    @PrimaryKey @ColumnInfo(name = CountryData.ID) val id: String,
    @ColumnInfo(name = CountryData.NAME) val name: String,
    @ColumnInfo(name = CountryData.TIME) val time: String,
    @ColumnInfo(name = CountryData.IMAGE) val imageUrl: String? = null
)
