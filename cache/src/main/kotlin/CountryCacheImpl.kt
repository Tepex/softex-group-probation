package com.softexlab.probation.cache

import com.softexlab.probation.data.CountryCache
import com.softexlab.probation.data.Country

import io.reactivex.Completable
import io.reactivex.Single

import timber.log.Timber

class CountryCacheImpl(private val db: CacheDatabase): CountryCache {
    override fun get(): Single<List<Country>> {
        Timber.d("DB.get()")
        return db.countryCachedDao().selectAll().map { countries -> countries.map { CountryMapper.from(it) } }
    }

    /*
    override fun replace(countries: List<Country>): Completable {
        Timber.d("DB.replace()")
        db.countryCachedDao().replaceAll(countries.map { CountryMapper.to(it) })
    }

    override fun delete(id: String): Completable {
        Timber.d("DB.delete($id)")
        db.countryCachedDao().delete(id)
    }
    */


    override fun replace(countries: List<Country>) = Completable.defer {
        Timber.d("DB.replace()")
        db.countryCachedDao()
            .insert(countries.map { CountryMapper.to(it) })
        Completable.complete()
    }

}
