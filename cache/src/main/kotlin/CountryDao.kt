package com.softexlab.probation.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update

import com.softexlab.probation.data.Country as CountryData 

import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface CountryDao {
    @Query("SELECT * FROM ${CountryData.ENTITY_NAME}")
    fun selectAll(): Single<List<Country>>
        
    @Query("DELETE FROM ${CountryData.ENTITY_NAME} WHERE ${CountryData.ID} = :id")
    fun delete(id: String): Completable
    
    @Query("SELECT COUNT(*) FROM ${CountryData.ENTITY_NAME}")
    fun count(): Single<Int>
    
    /*
    @Transaction
    fun replaceAll(countries: List<Country>): Completable {
        deleteAll()
        insert(countries)
    }
    */
    
    @Query("DELETE FROM ${CountryData.ENTITY_NAME}")
    fun deleteAll(): Completable
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(countries: List<Country>): Completable
}
