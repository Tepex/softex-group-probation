package com.softexlab.probation.cache

import com.softexlab.probation.data.Country as CountryData

object CountryMapper {
    fun from(cached: Country) = CountryData(
        id = cached.id,
        name = cached.name,
        time = cached.time,
        imageUrl = cached.imageUrl
    )
    
    fun to(data: CountryData) = Country(
        id = data.id,
        name = data.name,
        time = data.time,
        imageUrl = data.imageUrl
    )
}
