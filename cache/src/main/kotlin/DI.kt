package com.softexlab.probation.cache

import androidx.room.Room

import com.softexlab.probation.data.CountryCache

import org.koin.dsl.module

val cacheModule = module {
    single {
        Room.databaseBuilder(get(), CacheDatabase::class.java, "cache.db")
            .fallbackToDestructiveMigration()
            .build() 
    }
    
    single<CountryCache> { CountryCacheImpl(get()) }
}
