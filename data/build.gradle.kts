import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `java-library`
    kotlin("jvm")
    id(Kotlin.Serialization.id)
}

dependencies {
    implementation(kotlin(Kotlin.stdLib, KotlinCompilerVersion.VERSION))
    implementation(Kotlin.Serialization.runtime)

    implementation(Rx.rxJava)
    implementation(Rx.rxKotlin)
    /* DI Koin */
    implementation(Koin.coreDependency)
    
    implementation(project(Modules.domain.name))
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
