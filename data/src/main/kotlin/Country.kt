package com.softexlab.probation.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Country(
    @SerialName(ID) val id: String,
    @SerialName(NAME) val name: String,
    @SerialName(TIME) val time: String,
    @SerialName(IMAGE) val imageUrl: String? = null
) {
    companion object {
        const val ENTITY_NAME = "Country"
        const val ID = "Id"
        const val NAME = "Name"
        const val TIME = "Time"
        const val IMAGE = "Image"
    }

    override fun hashCode() = id.hashCode()

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Country) return false
        return other.id == id
    }
}
