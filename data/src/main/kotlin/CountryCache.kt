package com.softexlab.probation.data

import io.reactivex.Completable
import io.reactivex.Single

interface CountryCache {
    fun get(): Single<List<Country>>
    fun replace(countries: List<Country>): Completable
}
