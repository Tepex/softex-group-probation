package com.softexlab.probation.data

import io.reactivex.Single

interface CountryRemote {
    fun get(): Single<List<Country>>
}
