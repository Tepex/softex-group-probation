package com.softexlab.probation.data

import com.softexlab.probation.domain.Country as CountryDomain
import com.softexlab.probation.domain.CountryRepository

import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class CountryRepositoryImpl(
    private val remote: CountryRemote,
    private val cache: CountryCache
) : CountryRepository {

    private var memoryCache: MutableList<Country> = emptyList<Country>().toMutableList()

    override fun get(invalidateCache: Boolean): Single<List<CountryDomain>> {
        println("Data. get(invalidateCache = $invalidateCache)")
        val dataSource = getSource(invalidateCache)
        return map(dataSource.countries.flatMap { countries ->
            if (dataSource.isRemote) {
                memoryCache = countries.toMutableList()
//                cache.replace(countries)
            }
            Single.just(countries)
        })
    }

    override fun remove(id: String): Single<List<CountryDomain>> {
        memoryCache.remove(memoryCache.find { it.id == id })
//        cache.delete(id)
        return map(Single.just(memoryCache))
    }

    private fun getSource(invalidateCache: Boolean): DataSource =
        if (invalidateCache) {
            DataSource(remote.get(), true)
        } else if (memoryCache.isNotEmpty()) {
            DataSource(Single.just(memoryCache))
        }
        /*
        else if (cache.isNotEmpty().flatMap {  }) {
            println("Data.getSource: cacheDb")
            memoryCache = cache.get().toMutableList()
            DataSource(Single.just(memoryCache))
        }
        */
        else {
            DataSource(remote.get(), true)
        }

    private fun map(countries: Single<List<Country>>) =
        countries.map { list -> list.map { Mapper.map(it) } }

    data class DataSource(val countries: Single<List<Country>>, val isRemote: Boolean = false)
}
