package com.softexlab.probation.data

import com.softexlab.probation.domain.Country as CountryDomain

object Mapper {
    fun map(model: Country) =
        CountryDomain(
            id = model.id,
            name = model.name,
            time = model.time,
            imageUrl = model.imageUrl
        )
}
