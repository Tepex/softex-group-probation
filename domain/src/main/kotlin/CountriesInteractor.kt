package com.softexlab.probation.domain

import io.reactivex.Single

class CountriesInteractor(private val repository: CountryRepository) : CountriesUseCase {
    override fun get(invalidateCache: Boolean) = repository.get(invalidateCache)
    override fun remove(id: String) = repository.remove(id)
}
