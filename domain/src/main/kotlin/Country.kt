package com.softexlab.probation.domain

data class Country(
    val id: String,
    val name: String,
    val time: String,
    val imageUrl: String? = null
) {
    override fun hashCode() = id.hashCode()

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Country) return false
        return other.id == id
    }
}
