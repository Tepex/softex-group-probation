package com.softexlab.probation.domain

import io.reactivex.Single

interface CountryRepository {
    fun get(invalidateCache: Boolean = false): Single<List<Country>>
    fun remove(id: String): Single<List<Country>>
}
