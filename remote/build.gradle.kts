import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `java-library`
    kotlin("jvm")
}

dependencies {
    implementation(kotlin(Kotlin.stdLib, KotlinCompilerVersion.VERSION))
//    implementation(Kotlin.Serialization.runtime)
    /* DI Koin */
    implementation(Koin.coreDependency)
    implementation(Rx.rxJava)
    implementation(Retrofit.core)
    implementation(Retrofit.adapter)
    implementation(Retrofit.converter)
    implementation(Gson.dependency)
    implementation(OkHttp.okhttp)
    implementation(OkHttp.logging)
//    implementation(ThreeTen.dependency)

    implementation(project(Modules.data.name))
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
