package com.softexlab.probation.remote

import io.reactivex.Single

import retrofit2.http.GET
import retrofit2.http.Headers

interface Api {
    @GET("test.json")
    @Headers("Content-type: application/json")
    fun getCountries(): Single<List<Country>>
}
