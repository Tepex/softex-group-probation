package com.softexlab.probation.remote

import com.google.gson.annotations.SerializedName

import com.softexlab.probation.data.Country as CountryData

data class Country(
    @SerializedName(CountryData.ID) val id: String,
    @SerializedName(CountryData.NAME) val name: String,
    @SerializedName(CountryData.TIME) val time: String,
    @SerializedName(CountryData.IMAGE) val imageUrl: String?
) {
    override fun hashCode() = id.hashCode()

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Country) return false
        return other.id == id
    }
}
