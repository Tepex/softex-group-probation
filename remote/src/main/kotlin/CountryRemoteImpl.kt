package com.softexlab.probation.remote

import com.softexlab.probation.data.Country as CountryData
import com.softexlab.probation.data.CountryRemote

import java.util.concurrent.TimeUnit

const val TEST_DELAY = 5L

class CountryRemoteImpl(private val api: Api) : CountryRemote {
    override fun get() = api.getCountries()
//        .delay(TEST_DELAY, TimeUnit.SECONDS) /* Эмуляция лага */
        .map { it.map { item -> Mapper.map(item) } }
}
