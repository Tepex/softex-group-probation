package com.softexlab.probation.remote

import com.softexlab.probation.data.Country as CountryData

object Mapper {
    fun map(model: Country) =
        CountryData(
            id = model.id,
            name = model.name,
            time = model.time,
            imageUrl = model.imageUrl
        )
}
