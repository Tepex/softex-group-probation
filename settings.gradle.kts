rootProject.buildFileName = "build.gradle.kts"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "org.jetbrains.kotlin.jvm") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:${Kotlin.version}")
            }
            if (requested.id.id == Kotlin.Serialization.id) {
                useModule("org.jetbrains.kotlin:kotlin-serialization:${Kotlin.version}")
            }
        }
    }

    repositories {
        mavenCentral()
        jcenter()
        maven(url = "https://plugins.gradle.org/m2/")
    }
}

include(Modules.domain.name)
include(Modules.data.name)
include(Modules.remote.name)
include(Modules.cache.name)
include(Modules.app.name) 
